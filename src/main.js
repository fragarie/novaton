import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import { sync } from 'vuex-router-sync'

Vue.use(Vuex)
// Vue.config.productionTip = false

const store = new Vuex.Store({
	state: {
		isLoggedIn: true,
		local: true,
		id_facebook: 10155619214005049,
		name_facebook: 'Alan Gomes',
		endpoints: {
			
		}
	},
	mutations: {
		login (state, data) {
			state.id_facebook = data.id_facebook
			state.name_facebook = data.name_facebook
			state.isLoggedIn = true
		},
		logout (state) {
			state.id_facebook = false
			state.name_facebook = false
			state.isLoggedIn = false
		}
	}
})

sync(store, router)

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: { App }
})
