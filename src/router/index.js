import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Play from '@/components/Play'
import Ganaste from '@/components/Ganaste'

Vue.use(Router)

export default new Router({
  beforeEach (to, from, next)  {
    if (!store.state.isLoggedIn && !store.state.local)
      next('home')
    else
      next()
  },
  mode: 'history',
  routes: [
    {
      path: '/hello',
      name: 'hello',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/play',
      name: 'play',
      component: Play
  	},
  	{
      path: '/ganaste',
      name: 'ganaste',
      component: Ganaste
    }
  ]
})
